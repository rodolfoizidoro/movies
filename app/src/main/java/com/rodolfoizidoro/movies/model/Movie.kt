package com.rodolfoizidoro.movies.model

import java.io.Serializable

data class Movie(
    val id: Int = 0,
    val title: String = "",
    val releaseDate: String = "",
    val genreIds: List<Int> = emptyList(),
    val overview: String = "",
    val backdropPath: String = ""
) : Serializable {
    fun getPosterUrl() : String = "https://image.tmdb.org/t/p/w500$backdropPath"
    var genre : List<Genre> = emptyList()
}

