package com.rodolfoizidoro.movies.data.remote

import com.rodolfoizidoro.movies.BuildConfig
import com.rodolfoizidoro.movies.model.GenreListResponse
import com.rodolfoizidoro.movies.model.MovieListResponse
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

class MovieRepositoryImpl(private val service: MovieService) : MovieRepository {
    companion object {
        const val API_KEY = BuildConfig.API_KEY
        const val language = "pt-BR"
    }

    override suspend fun getMoviesUpcomingAsync(): Deferred<MovieListResponse> {
        return withContext(Dispatchers.IO) {
            async { service.getUpcomingAsync(API_KEY, language).await() }
        }
    }

    override suspend fun getMoviesTopRatedAsync(): Deferred<MovieListResponse> {
        return withContext(Dispatchers.IO) {
            async { service.getTopRatedAsync(API_KEY, language).await() }
        }
    }

    override suspend fun getMoviesPopularAsync(): Deferred<MovieListResponse> {
        return withContext(Dispatchers.IO) {
            async { service.getPopularAsync(API_KEY, language).await() }
        }
    }

    override suspend fun getGenresAsync(): Deferred<GenreListResponse> {
        return withContext(Dispatchers.IO) {
            async { service.findGenresAsync(API_KEY, language).await() }
        }
    }

    override suspend fun searchMoviesAsync(query: String): Deferred<MovieListResponse> {
        return withContext(Dispatchers.IO) {
            async { service.searchMoviesAsync(query, API_KEY, language).await() }
        }
    }
}
