package com.rodolfoizidoro.movies.model

data class MovieListResponse(val results: List<Movie> = emptyList())
