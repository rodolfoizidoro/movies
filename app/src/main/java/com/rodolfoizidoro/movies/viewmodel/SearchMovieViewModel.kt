package com.rodolfoizidoro.movies.viewmodel

import com.rodolfoizidoro.movies.common.StateLiveData
import com.rodolfoizidoro.movies.common.StateMutableLiveData
import com.rodolfoizidoro.movies.common.base.BaseViewModel
import com.rodolfoizidoro.movies.data.remote.MovieRepository
import com.rodolfoizidoro.movies.model.Movie
import kotlinx.coroutines.launch

class SearchMovieViewModel(private val repository: MovieRepository) : BaseViewModel() {

    private val mMovies = StateMutableLiveData<List<Movie>, Throwable>()
    val movies: StateLiveData<List<Movie>, Throwable> get() = mMovies

    fun searchMovie(query: String) {
        jobs add launch {
            mMovies.loading.value = true
            try {
                val response = repository.searchMoviesAsync(query).await().results
                val genres = repository.getGenresAsync().await().genres

                response.forEach { movie ->
                    movie.genre = genres.filter { movie.genreIds.contains(it.id) }
                }

                mMovies.success.value = response.sortedByDescending { it.releaseDate }
            } catch (t: Throwable) {
                mMovies.error.value = t
            } finally {
                mMovies.loading.value = false
            }
        }
    }
}
