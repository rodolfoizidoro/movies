package com.rodolfoizidoro.movies.model

import java.io.Serializable

data class Genre(val id : Int = 0 , val name : String = "") : Serializable
