package com.rodolfoizidoro.movies.data.remote

import com.rodolfoizidoro.movies.model.GenreListResponse
import com.rodolfoizidoro.movies.model.Movie
import com.rodolfoizidoro.movies.model.MovieListResponse
import kotlinx.coroutines.Deferred

interface MovieRepository {
    suspend fun getMoviesUpcomingAsync(): Deferred<MovieListResponse>
    suspend fun getMoviesTopRatedAsync(): Deferred<MovieListResponse>
    suspend fun getMoviesPopularAsync(): Deferred<MovieListResponse>
    suspend fun getGenresAsync(): Deferred<GenreListResponse>
    suspend fun searchMoviesAsync(query: String): Deferred<MovieListResponse>
}
