package com.rodolfoizidoro.movies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.rodolfoizidoro.movies.data.remote.MovieRepository
import com.rodolfoizidoro.movies.model.*
import com.rodolfoizidoro.movies.viewmodel.MovieListViewModel
import io.mockk.*
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class MovieListViewModelTest {

    @get:Rule
    val instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val repository = mockk<MovieRepository>()
    val subject = MovieListViewModel(repository)

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `Should contains 1 top rated movie with 1 genre`() {
        coEvery {
            repository.getMoviesTopRatedAsync().await()
        } returns MovieListResponse(listOf(Movie(genreIds = listOf(1))))
        coEvery { repository.getGenresAsync().await() } returns GenreListResponse(listOf(Genre(id = 1, name = "Drama")))

        subject.findMovies(Category.TopRated)


        coVerifyAll {
            repository.getMoviesTopRatedAsync().await()
            repository.getGenresAsync().await()
        }

        val list = subject.movies.successLiveData.test().awaitValue().value()

        assert(list.size == 1)
        with(list.first()) {
            assertEquals("Drama", this.genre.first().name)
            assertEquals(1, this.genre.first().id)
        }
    }

    @Test
    fun `Should call exception when returns error`() {
        subject.findMovies(Category.TopRated)

        subject.movies.errorLiveData.test().awaitValue().assertHasValue()
    }

    @Test
    fun `Should call popular movies when send category popular`() {
        coEvery {
            repository.getMoviesPopularAsync().await()
        } returns MovieListResponse(listOf(Movie(genreIds = listOf(1))))
        coEvery { repository.getGenresAsync().await() } returns GenreListResponse(listOf(Genre(id = 1, name = "Drama")))

        subject.findMovies(Category.Popular)

        coVerifyAll {
            repository.getMoviesPopularAsync().await()
            repository.getGenresAsync().await()
        }

        coVerify(exactly = 0){
            repository.getMoviesTopRatedAsync().await()
            repository.getMoviesUpcomingAsync().await()
        }
    }

    @Test
    fun `Should call upcoming movies when send category upcoming`() {
        coEvery {
            repository.getMoviesUpcomingAsync().await()
        } returns MovieListResponse(listOf(Movie(genreIds = listOf(1))))
        coEvery { repository.getGenresAsync().await() } returns GenreListResponse(listOf(Genre(id = 1, name = "Drama")))

        subject.findMovies(Category.Upcoming)

        coVerifyAll {
            repository.getMoviesUpcomingAsync().await()
            repository.getGenresAsync().await()
        }

        coVerify(exactly = 0){
            repository.getMoviesTopRatedAsync().await()
            repository.getMoviesPopularAsync().await()
        }
    }
}
