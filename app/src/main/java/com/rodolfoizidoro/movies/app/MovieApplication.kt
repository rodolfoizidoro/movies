package com.rodolfoizidoro.movies.app

import android.app.Application
import com.rodolfoizidoro.movies.di.DataModule
import com.rodolfoizidoro.movies.di.MovieDetailModule
import com.rodolfoizidoro.movies.di.MovieListModule
import com.rodolfoizidoro.movies.di.SearchMovieModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MovieApplication)
            modules(listOf(DataModule.module, MovieListModule.module, MovieDetailModule.module, SearchMovieModule.module))
        }
    }
}
