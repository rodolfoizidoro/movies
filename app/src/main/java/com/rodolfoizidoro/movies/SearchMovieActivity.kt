package com.rodolfoizidoro.movies

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.rodolfoizidoro.movies.adapter.MoviesAdapter
import com.rodolfoizidoro.movies.common.extension.toast
import com.rodolfoizidoro.movies.flow.FlowController
import com.rodolfoizidoro.movies.viewmodel.MovieListViewModel
import com.rodolfoizidoro.movies.viewmodel.SearchMovieViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class SearchMovieActivity : AppCompatActivity() {
    private val viewModel by viewModel<SearchMovieViewModel>()
    private val flowController = FlowController(this)

    companion object {
        const val EXTRA_QUERY = "extra_ query"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movie)
        setupRecyclerView()

        viewModel.movies.observe(this,
            onSuccess = { list ->
                rvMovies.adapter = MoviesAdapter(list) {
                    flowController.openMovieDetail(it)
                }
            },
            onError = {
                toast(getString(R.string.generic_error))
            })

        supportActionBar?.title = getQuery()

        viewModel.searchMovie(getQuery())
    }

    private fun getQuery(): String = intent.getStringExtra(EXTRA_QUERY) ?: ""

    private fun setupRecyclerView() {
        rvMovies.layoutManager = LinearLayoutManager(this)
    }
}
