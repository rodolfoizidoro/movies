package com.rodolfoizidoro.movies.flow

import android.app.Activity
import android.content.Intent
import com.rodolfoizidoro.movies.MovieDetailActivity
import com.rodolfoizidoro.movies.SearchMovieActivity
import com.rodolfoizidoro.movies.model.Movie

class FlowController(private val activity: Activity) {

    fun openMovieDetail(movie: Movie) {
        val intent = Intent(activity, MovieDetailActivity::class.java).apply {
            putExtra(MovieDetailActivity.EXTRA_MOVIE, movie)
        }
        activity.startActivity(intent)
    }

    fun openSearchMovie(query: String?) {
        val intent = Intent(activity, SearchMovieActivity::class.java).apply {
            putExtra(SearchMovieActivity.EXTRA_QUERY, query)
        }
        activity.startActivity(intent)
    }
}
