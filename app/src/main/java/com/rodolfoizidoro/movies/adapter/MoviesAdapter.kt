package com.rodolfoizidoro.movies.adapter

import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.rodolfoizidoro.movies.R
import com.rodolfoizidoro.movies.common.extension.inflate
import com.rodolfoizidoro.movies.databinding.MovieListItemBinding
import com.rodolfoizidoro.movies.model.Movie

class MoviesAdapter(
    private val items: List<Movie>,
    private val onClick: (Movie) -> Unit
) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.movie_list_item)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.run {
            with(items[position]) {
                item = this
                executePendingBindings()
                root.setOnClickListener { onClick(this) }
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = DataBindingUtil.bind<MovieListItemBinding>(itemView)
    }
}
