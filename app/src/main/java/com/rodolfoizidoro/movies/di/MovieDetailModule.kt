package com.rodolfoizidoro.movies.di

import com.rodolfoizidoro.movies.model.Movie
import com.rodolfoizidoro.movies.viewmodel.MovieDetailViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object MovieDetailModule {
    val module = module {
        viewModel {(movie: Movie) -> MovieDetailViewModel(movie) }
    }
}
