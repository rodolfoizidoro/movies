package com.rodolfoizidoro.movies

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.rodolfoizidoro.movies.common.base.BaseActivity
import com.rodolfoizidoro.movies.databinding.ActivityMovieDetailBinding
import com.rodolfoizidoro.movies.model.Movie
import com.rodolfoizidoro.movies.viewmodel.MovieDetailViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MovieDetailActivity : BaseActivity() {

    companion object {
        const val EXTRA_MOVIE = "extra_movie"
    }

    private val mViewModel: MovieDetailViewModel by viewModel { parametersOf(getExtraMovie()) }
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMovieDetailBinding>(this, R.layout.activity_movie_detail)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = getExtraMovie().title
        binding.viewModel = mViewModel
        binding.lifecycleOwner = this
    }

    override fun onBackPressed() {
        finish()
    }

    private fun getExtraMovie() = intent.getSerializableExtra(EXTRA_MOVIE) as Movie
}
