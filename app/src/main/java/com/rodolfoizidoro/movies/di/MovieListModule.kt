package com.rodolfoizidoro.movies.di

import com.rodolfoizidoro.movies.data.remote.MovieRepository
import com.rodolfoizidoro.movies.data.remote.MovieRepositoryImpl
import com.rodolfoizidoro.movies.viewmodel.MovieListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object MovieListModule {
    val module = module {
        single<MovieRepository> { MovieRepositoryImpl(get()) }
        viewModel { MovieListViewModel(get()) }
    }
}
