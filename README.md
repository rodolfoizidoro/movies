# Movies

## Arquitetura do App

* Utilizei a arquitetura MVVM para as View ficarem somente com a responsabilidade de visualização, o ViewModel cuida de toda a lógica de negócio, os Repositórios de toda a parte de buscar os dados.
* Activitys:
    A unica responsabilidades das activitys é montar as telas, listas, somente cuidando da parte visual do app
* ViewModels: 
    Cuida de toda a lógica do app, no caso saber onde buscar as informações, publicar as informações para a activity por meio dos LiveDatas, é a camada central do app.
* Repository/Data:
    Camada responsável por buscar as informações, seja local, ou em uma api. 
* Models
    São os modelos de negócio do app.
* Coroutines
    Eu prefiro Coroutines ao invés do ReactiveX para lidar com tarefas assincronas, pois a forma sequencial fica mais claro para mim do que os Callbacks do RX.
* Koin 
    Para injeção de dependência eu gosto muito do Koin ao invés do Dagger e Kodein, ele é muito simples de utilizar e a versão 2.0 ganhou muita perfomace e ele combina muito bem com viewModels.
* Databinding
    Eu particularmente não gosto de databinding, ele é amado e odiado, para mim ele funciona muito bem com recyclerviews, apesar que quando dá problemas é horrível de encontrar a causa. mas ele tem seus ganhos.
* Mockk
    Acho uma biblioteca fundamental para testes em kotlin, é bem idiomático e simples de utilizar, o mockito encontrei alguns casos que não se adaptava muito bem com kotlin, principalmente com objetos que não podem ser nullos, surgiu o mockito kotlin mas o MockK acho mais completo.
* StateLiveData e MutableLiveData
    Eu criei essas classes para facilitar o uso dos livedata em caso que continham 3 estados, apesar que no teste eu não uitlizei o estado de progress, nesse artigo eu explico melhor as minhas dores e o que me faz criar ele:
    https://labs.getninjas.com.br/android-viewmodel-livedata-boilerplate-4b6079577cd6
* Tests 
    Eu criei um teste unitário e comecei a criar um teste visual. Para mim os testes são muito importantes em um app pois permitem a refatoração do aplicativo sem problemas.
* Observações
    Eu sei que o projeto tem muitas bibliotecas que não utilzei, eu geralmente pego elas de um projeto que ja tenho, e também para fazer experimentos.
* Camada de cache
    Eu poderia fazer a camada de cache/offline com Room, mas com a complexidade sendo baixa, o retrofit lida com caches muito bem, para mim não foi necessário fazer toda a camada de cache.
* Melhorias
   -  Algo que me incomoda seria a parte de Genres(Ação, Comédia, Drama) que dificilmente eles vão mudar, eu sei que o retrofit tem cache, mas essa parte eu acho que poderia ter ficado local, e fazer algum mecanismo de salvar 1 vez quando ouver alteração, ou quando abrir o app.algo nesse sentido, pois dificilmente irão surgir novos generos toda semana.

   -  Eu poderia ter melhorado o visual do app, colocado as animações de transição, feito a imagem de fundo com collapsing toolbar, talvez eu faça no futuro e continue trabalhando no app para treinar minhas habilidades.
    -  Commits, eu apenas fiz o clone para o GitLab pois não queria deixar o repositorio publico em meu github pois também utilizo no trabalho, todos os commits foram feitos em um repósitorio privado.



# Perguntas

1) Principio da Responsabilidade única.
O principio da responsabilidade única é que uma classe, função, objeto, tenha um propósito único, que os atores são muito bem definidos, e que há somente um motivo para altera-la , que é quando os requisitos mudam.
Ex: Uma função soma, que faz a soma, faz chama de rede, salva no banco de dados, ela está fazendo muito mais coisas que ela deveria fazer.
Assim como uma classe Empregado, que tem métodos como Salvar, Descrever, Calcular Salário.


2) Código Limpo.
Um bom código é aquele que é escalavel, testavel e fica muito claro o que está fazendo, Sem efeitos colaterais. classes separadas por suas responsabilidades, separado em modulos para ser reutilizavel. Com um propósito bem claro.
Que seja fácil de dar manutenção e não seja custoso de fazer novas implementações.
Um código limpo deveria ter comentários? Não sei, pois métodos claros e auto explicativos os comentários seriam redudantes, mas para pessoas novas e fora de contexto iriam facilitar muito.
Resumindo se fosse para escolher 3 principais qualidades para um código limpo seria:
1 - Testável.
2 - Facil manutenção.
3 - Escalável
