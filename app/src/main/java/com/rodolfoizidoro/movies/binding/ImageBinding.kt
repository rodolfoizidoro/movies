package com.rodolfoizidoro.movies.binding

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.rodolfoizidoro.movies.R

object ImageBinding {
    @JvmStatic
    @BindingAdapter("app:imageUrl", "app:placeHolder", requireAll = false)
    fun setImageUrl(imageView: ImageView, url: String?, placeholder: Drawable?) {
        Glide
            .with(imageView.context)
            .load(url)
            .placeholder(placeholder)
            .into(imageView)
    }
}
