package com.rodolfoizidoro.movies.viewmodel

import com.rodolfoizidoro.movies.common.base.BaseViewModel
import com.rodolfoizidoro.movies.data.remote.MovieRepository
import com.rodolfoizidoro.movies.model.Movie

class MovieDetailViewModel(val movie: Movie) : BaseViewModel()
