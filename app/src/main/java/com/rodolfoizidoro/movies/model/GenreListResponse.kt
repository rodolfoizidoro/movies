package com.rodolfoizidoro.movies.model

data class GenreListResponse(val genres: List<Genre> = emptyList())
