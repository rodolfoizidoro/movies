package com.rodolfoizidoro.movies

import android.os.Bundle
import android.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.rodolfoizidoro.movies.adapter.MoviesAdapter
import com.rodolfoizidoro.movies.common.base.BaseActivity
import com.rodolfoizidoro.movies.common.extension.toast
import com.rodolfoizidoro.movies.flow.FlowController
import com.rodolfoizidoro.movies.model.Category
import com.rodolfoizidoro.movies.viewmodel.MovieListViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {
    private val viewModel by viewModel<MovieListViewModel>()
    private val flowController = FlowController(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()

        viewModel.movies.observe(this,
            onSuccess = { list ->
                rvMovies.adapter = MoviesAdapter(list) {
                    flowController.openMovieDetail(it)
                }
            },
            onError = {
                toast(getString(R.string.generic_error))
            })

        chipPopular.setOnClickListener {
            viewModel.findMovies(Category.Popular)
        }
        chipTopRated.setOnClickListener {
            viewModel.findMovies(Category.TopRated)
        }
        chipUpcoming.setOnClickListener {
            viewModel.findMovies(Category.Upcoming)
        }

        svMovies.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                flowController.openSearchMovie(query)
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean  = false
        } )

        viewModel.findMovies(Category.Upcoming)
    }

    private fun setupRecyclerView() {
        rvMovies.layoutManager = LinearLayoutManager(this)
    }
}
