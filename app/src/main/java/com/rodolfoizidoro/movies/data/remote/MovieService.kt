package com.rodolfoizidoro.movies.data.remote

import com.rodolfoizidoro.movies.model.GenreListResponse
import com.rodolfoizidoro.movies.model.MovieListResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    @GET("/3/movie/upcoming")
    fun getUpcomingAsync(
        @Query("api_key") key: String,
        @Query("language") language: String
        ): Deferred<MovieListResponse>

    @GET("/3/movie/top_rated")
    fun getTopRatedAsync(
        @Query("api_key") key: String,
        @Query("language") language: String
    ): Deferred<MovieListResponse>

    @GET("/3/movie/popular")
    fun getPopularAsync(
        @Query("api_key") key: String,
        @Query("language") language: String
    ): Deferred<MovieListResponse>

    @GET("/3/genre/movie/list")
    fun findGenresAsync(
        @Query("api_key") key: String,
        @Query("language") language: String
    ): Deferred<GenreListResponse>

    @GET("/3/search/movie")
    fun searchMoviesAsync(
        @Query("query") query: String,
        @Query("api_key") key: String,
        @Query("language") language: String
    ): Deferred<MovieListResponse>
}
