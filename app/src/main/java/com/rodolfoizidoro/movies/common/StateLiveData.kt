package com.rodolfoizidoro.movies.common

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

interface StateLiveData<R, E> {
    fun observe(owner: LifecycleOwner, onSuccess: (R) -> Unit, onError: (E) -> Unit = {}, onLoading: (Boolean) -> Unit = {})
    val loadingLiveData: LiveData<Boolean>
    val successLiveData: LiveData<R>
    val errorLiveData: LiveData<E>
}
