package com.rodolfoizidoro.movies

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.rodolfoizidoro.movies.adapter.MoviesAdapter
import junit.framework.Assert.assertTrue
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java, true, false)

    @Test
    fun checkChipUpComingIsCheckedWhenLaunch(){
        activityRule.launchActivity(null)
        assertTrue(activityRule.activity.chipUpcoming.isCheckedIconVisible)

        onView(withId(R.id.rvMovies)).perform(actionOnItemAtPosition<MoviesAdapter.ViewHolder>(0, click()))
    }
}
