package com.rodolfoizidoro.movies.model

sealed class Category {
    object Popular : Category()
    object TopRated : Category()
    object Upcoming : Category()
}
