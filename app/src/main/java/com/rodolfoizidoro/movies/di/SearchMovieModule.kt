package com.rodolfoizidoro.movies.di

import com.rodolfoizidoro.movies.viewmodel.SearchMovieViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object SearchMovieModule {
    val module = module {
        viewModel { SearchMovieViewModel(get()) }
    }
}
