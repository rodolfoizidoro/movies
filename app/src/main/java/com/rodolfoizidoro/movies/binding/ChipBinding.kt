package com.rodolfoizidoro.movies.binding

import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.rodolfoizidoro.movies.model.Genre

object ChipBinding {
    @JvmStatic
    @BindingAdapter("app:chips")
    fun setChips(chipGroup: ChipGroup, chips : List<Genre>) {
        chipGroup.removeAllViews()
        chips.forEach {
            val chip = Chip(chipGroup.context)
            chip.isEnabled = false
            chip.text = it.name
            chip.setTextColor(ContextCompat.getColor(chipGroup.context, android.R.color.black))
            chipGroup.addView(chip)
        }
    }
}
