package com.rodolfoizidoro.movies.common.base

import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

   override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
               onToolbarBackPressed()
                return true
            }
        }
        return false
    }

    fun onToolbarBackPressed() {
        finish()
    }
}
